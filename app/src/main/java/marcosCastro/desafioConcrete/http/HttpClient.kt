package marcosCastro.desafioConcrete.http

import android.arch.lifecycle.MutableLiveData
import marcosCastro.desafioConcrete.model.JavaRepository
import marcosCastro.desafioConcrete.model.Pull
import marcosCastro.desafioConcrete.model.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HttpClient private constructor() {

    companion object Singleton {
        val INSTANCE: HttpClient by lazy { HttpClient() }

        const val BASE_URL = "https://api.github.com/"
    }

    val repositoryHandler: ApiService

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        repositoryHandler = retrofit.create(ApiService::class.java)
    }

    fun getRepo(value: Int?, javaRepository: MutableLiveData<ApiResponse>, values: MutableList<Repository>) {
        repositoryHandler.getJavaRepos(value).enqueue(object : Callback<JavaRepository> {
            override fun onResponse(call: Call<JavaRepository>?, response: Response<JavaRepository>) {

                if (response.isSuccessful) {
                    values.addAll(response.body()?.items!!)

                    javaRepository.postValue(ApiResponse(values))

                } else {
                    javaRepository.postValue(ApiResponse(Throwable()))
                }
            }

            override fun onFailure(call: Call<JavaRepository>?, t: Throwable?) {
                javaRepository.postValue(ApiResponse(Throwable()))

            }

        })
    }

    fun getJavaPR(user: String, repo: String, mApiResponse: MutableLiveData<ApiResponse>) {

        HttpClient.INSTANCE.repositoryHandler.getJavaPR(user, repo).enqueue(object : Callback<List<Pull>> {
            override fun onResponse(call: Call<List<Pull>>?, response: Response<List<Pull>>) {

                if (response.isSuccessful) {
                    mApiResponse.postValue(ApiResponse(response.body()!!))

                } else {
                    mApiResponse.postValue(ApiResponse(Throwable()))
                }

            }

            override fun onFailure(call: Call<List<Pull>>?, t: Throwable?) {
                mApiResponse.postValue(ApiResponse(Throwable()))
            }

        })

    }
}