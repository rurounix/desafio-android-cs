package marcosCastro.desafioConcrete.http

import marcosCastro.desafioConcrete.model.JavaRepository
import marcosCastro.desafioConcrete.model.Pull
import marcosCastro.desafioConcrete.model.Repository
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET("search/repositories?q=language:Java&sort=stars")
    fun getJavaRepos(@Query("page") page: Int? = null): Call<JavaRepository>

    @GET("repos/{owner}/{repo}/pulls?state=all")
    fun getJavaPR(@Path("owner") owner: String, @Path("repo") repo: String): Call<List<Pull>>
}