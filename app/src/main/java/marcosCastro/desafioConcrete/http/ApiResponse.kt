package marcosCastro.desafioConcrete.http

/**
 * Created by msn-d on 31/01/2018.
 */
class ApiResponse {
    var data: Any? = null
    var error: Throwable? = null

    constructor(data: Any) {
        this.data = data
        this.error = null
    }

    constructor(error: Throwable) {
        this.error = error
        this.data = null
    }
}