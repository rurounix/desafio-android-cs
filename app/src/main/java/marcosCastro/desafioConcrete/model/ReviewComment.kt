package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class ReviewComment(
        @SerializedName("href") val href: String //https://api.github.com/repos/spring-projects/spring-boot/pulls/comments{/number}
)