package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Statuses(
        @SerializedName("href") val href: String //https://api.github.com/repos/spring-projects/spring-boot/statuses/2eb75020a93bf28153b757605fb616e8a563c08a
)