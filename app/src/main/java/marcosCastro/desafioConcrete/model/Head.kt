package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Head(
        @SerializedName("label") val label: String, //bclozel:gh-7475
        @SerializedName("ref") val ref: String, //gh-7475
        @SerializedName("sha") val sha: String, //2eb75020a93bf28153b757605fb616e8a563c08a
        @SerializedName("user") val user: Owner,
        @SerializedName("repo") val repo: Repository
)