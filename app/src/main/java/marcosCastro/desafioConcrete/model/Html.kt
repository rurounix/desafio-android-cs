package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Html(
        @SerializedName("href") val href: String //https://github.com/spring-projects/spring-boot/pull/7654
)