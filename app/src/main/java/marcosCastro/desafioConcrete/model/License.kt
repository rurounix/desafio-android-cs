package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class License(
        @SerializedName("key") val key: String, //apache-2.0
        @SerializedName("name") val name: String, //Apache License 2.0
        @SerializedName("spdx_id") val spdxId: String, //Apache-2.0
        @SerializedName("url") val url: String //https://api.github.com/licenses/apache-2.0
)