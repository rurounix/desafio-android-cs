package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Comments(
        @SerializedName("href") val href: String //https://api.github.com/repos/spring-projects/spring-boot/issues/7654/comments
)