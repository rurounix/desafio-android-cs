package marcosCastro.desafioConcrete.model
import com.google.gson.annotations.SerializedName



data class JavaRepository(
		@SerializedName("total_count") val totalCount: Int, //4179271
		@SerializedName("incomplete_results") val incompleteResults: Boolean, //false
		@SerializedName("items") val items: MutableList<Repository>
)

