package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Pull(
        @SerializedName("url") val url: String, //https://api.github.com/repos/spring-projects/spring-boot/pulls/7654
        @SerializedName("id") val id: Int, //98119207
        @SerializedName("html_url") val htmlUrl: String, //https://github.com/spring-projects/spring-boot/pull/7654
        @SerializedName("diff_url") val diffUrl: String, //https://github.com/spring-projects/spring-boot/pull/7654.diff
        @SerializedName("patch_url") val patchUrl: String, //https://github.com/spring-projects/spring-boot/pull/7654.patch
        @SerializedName("issue_url") val issueUrl: String, //https://api.github.com/repos/spring-projects/spring-boot/issues/7654
        @SerializedName("number") val number: Int, //7654
        @SerializedName("state") val state: String, //open
        @SerializedName("locked") val locked: Boolean, //false
        @SerializedName("title") val title: String, //Introduce @ConditionalOnMissingServletFilter
        @SerializedName("user") val user: Owner,
        @SerializedName("body") val body: String,
        @SerializedName("created_at") val createdAt: String, //2016-12-15T10:02:28Z
        @SerializedName("updated_at") val updatedAt: String, //2017-06-28T15:37:15Z
        @SerializedName("closed_at") val closedAt: Any, //null
        @SerializedName("merged_at") val mergedAt: Any, //null
        @SerializedName("merge_commit_sha") val mergeCommitSha: String, //807ae5c1b1dafede3533867671f392026f089b13
        @SerializedName("assignee") val assignee: Any, //null
        @SerializedName("assignees") val assignees: List<Any>,
        @SerializedName("requested_reviewers") val requestedReviewers: List<Any>,
        @SerializedName("requested_teams") val requestedTeams: List<Any>,
        @SerializedName("milestone") val milestone: Any, //null
        @SerializedName("commits_url") val commitsUrl: String, //https://api.github.com/repos/spring-projects/spring-boot/pulls/7654/commits
        @SerializedName("review_comments_url") val reviewCommentsUrl: String, //https://api.github.com/repos/spring-projects/spring-boot/pulls/7654/comments
        @SerializedName("review_comment_url") val reviewCommentUrl: String, //https://api.github.com/repos/spring-projects/spring-boot/pulls/comments{/number}
        @SerializedName("comments_url") val commentsUrl: String, //https://api.github.com/repos/spring-projects/spring-boot/issues/7654/comments
        @SerializedName("statuses_url") val statusesUrl: String, //https://api.github.com/repos/spring-projects/spring-boot/statuses/2eb75020a93bf28153b757605fb616e8a563c08a
        @SerializedName("head") val head: Head,
        @SerializedName("base") val base: Base,
        @SerializedName("_links") val links: Links,
        @SerializedName("author_association") val authorAssociation: String //MEMBER
) {
    fun isClosed() = state == "closed"
}