package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Repository(
        @SerializedName("id") val id: Int, //7508411
        @SerializedName("name") val name: String, //RxJava
        @SerializedName("full_name") val fullName: String, //ReactiveX/RxJava
        @SerializedName("owner") val owner: Owner,
        @SerializedName("private") val private: Boolean, //false
        @SerializedName("html_url") val htmlUrl: String, //https://github.com/ReactiveX/RxJava
        @SerializedName("description") val description: String, //RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.
        @SerializedName("fork") val fork: Boolean, //false
        @SerializedName("url") val url: String, //https://api.github.com/repos/ReactiveX/RxJava
        @SerializedName("forks_url") val forksUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/forks
        @SerializedName("keys_url") val keysUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/keys{/key_id}
        @SerializedName("collaborators_url") val collaboratorsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/collaborators{/collaborator}
        @SerializedName("teams_url") val teamsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/teams
        @SerializedName("hooks_url") val hooksUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/hooks
        @SerializedName("issue_events_url") val issueEventsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/issues/events{/number}
        @SerializedName("events_url") val eventsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/events
        @SerializedName("assignees_url") val assigneesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/assignees{/user}
        @SerializedName("branches_url") val branchesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/branches{/branch}
        @SerializedName("tags_url") val tagsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/tags
        @SerializedName("blobs_url") val blobsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/git/blobs{/sha}
        @SerializedName("git_tags_url") val gitTagsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/git/tags{/sha}
        @SerializedName("git_refs_url") val gitRefsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/git/refs{/sha}
        @SerializedName("trees_url") val treesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/git/trees{/sha}
        @SerializedName("statuses_url") val statusesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/statuses/{sha}
        @SerializedName("languages_url") val languagesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/languages
        @SerializedName("stargazers_url") val stargazersUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/stargazers
        @SerializedName("contributors_url") val contributorsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/contributors
        @SerializedName("subscribers_url") val subscribersUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/subscribers
        @SerializedName("subscription_url") val subscriptionUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/subscription
        @SerializedName("commits_url") val commitsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/commits{/sha}
        @SerializedName("git_commits_url") val gitCommitsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/git/commits{/sha}
        @SerializedName("comments_url") val commentsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/comments{/number}
        @SerializedName("issue_comment_url") val issueCommentUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/issues/comments{/number}
        @SerializedName("contents_url") val contentsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/contents/{+path}
        @SerializedName("compare_url") val compareUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/compare/{base}...{head}
        @SerializedName("merges_url") val mergesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/merges
        @SerializedName("archive_url") val archiveUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/{archive_format}{/ref}
        @SerializedName("downloads_url") val downloadsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/downloads
        @SerializedName("issues_url") val issuesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/issues{/number}
        @SerializedName("pulls_url") val pullsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}
        @SerializedName("milestones_url") val milestonesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/milestones{/number}
        @SerializedName("notifications_url") val notificationsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/notifications{?since,all,participating}
        @SerializedName("labels_url") val labelsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/labels{/name}
        @SerializedName("releases_url") val releasesUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/releases{/id}
        @SerializedName("deployments_url") val deploymentsUrl: String, //https://api.github.com/repos/ReactiveX/RxJava/deployments
        @SerializedName("created_at") val createdAt: String, //2013-01-08T20:11:48Z
        @SerializedName("updated_at") val updatedAt: String, //2018-01-29T22:37:14Z
        @SerializedName("pushed_at") val pushedAt: String, //2018-01-28T13:31:38Z
        @SerializedName("git_url") val gitUrl: String, //git://github.com/ReactiveX/RxJava.git
        @SerializedName("ssh_url") val sshUrl: String, //git@github.com:ReactiveX/RxJava.git
        @SerializedName("clone_url") val cloneUrl: String, //https://github.com/ReactiveX/RxJava.git
        @SerializedName("svn_url") val svnUrl: String, //https://github.com/ReactiveX/RxJava
        @SerializedName("homepage") val homepage: String,
        @SerializedName("size") val size: Int, //48000
        @SerializedName("stargazers_count") val stargazersCount: Int, //30513
        @SerializedName("watchers_count") val watchersCount: Int, //30513
        @SerializedName("language") val language: String, //Java
        @SerializedName("has_issues") val hasIssues: Boolean, //true
        @SerializedName("has_projects") val hasProjects: Boolean, //true
        @SerializedName("has_downloads") val hasDownloads: Boolean, //true
        @SerializedName("has_wiki") val hasWiki: Boolean, //true
        @SerializedName("has_pages") val hasPages: Boolean, //true
        @SerializedName("forks_count") val forksCount: Int, //5364
        @SerializedName("mirror_url") val mirrorUrl: Any, //null
        @SerializedName("archived") val archived: Boolean, //false
        @SerializedName("open_issues_count") val openIssuesCount: Int, //34
        @SerializedName("license") val license: License,
        @SerializedName("forks") val forks: Int, //5364
        @SerializedName("open_issues") val openIssues: Int, //34
        @SerializedName("watchers") val watchers: Int, //30513
        @SerializedName("default_branch") val defaultBranch: String, //2.x
        @SerializedName("score") val score: Double //1.0
)