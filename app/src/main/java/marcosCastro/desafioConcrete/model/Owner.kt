package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Owner(
        @SerializedName("login") val login: String, //ReactiveX
        @SerializedName("id") val id: Int, //6407041
        @SerializedName("avatar_url") val avatarUrl: String, //https://avatars1.githubusercontent.com/u/6407041?v=4
        @SerializedName("gravatar_id") val gravatarId: String,
        @SerializedName("url") val url: String, //https://api.github.com/users/ReactiveX
        @SerializedName("html_url") val htmlUrl: String, //https://github.com/ReactiveX
        @SerializedName("followers_url") val followersUrl: String, //https://api.github.com/users/ReactiveX/followers
        @SerializedName("following_url") val followingUrl: String, //https://api.github.com/users/ReactiveX/following{/other_user}
        @SerializedName("gists_url") val gistsUrl: String, //https://api.github.com/users/ReactiveX/gists{/gist_id}
        @SerializedName("starred_url") val starredUrl: String, //https://api.github.com/users/ReactiveX/starred{/owner}{/repo}
        @SerializedName("subscriptions_url") val subscriptionsUrl: String, //https://api.github.com/users/ReactiveX/subscriptions
        @SerializedName("organizations_url") val organizationsUrl: String, //https://api.github.com/users/ReactiveX/orgs
        @SerializedName("repos_url") val reposUrl: String, //https://api.github.com/users/ReactiveX/repos
        @SerializedName("events_url") val eventsUrl: String, //https://api.github.com/users/ReactiveX/events{/privacy}
        @SerializedName("received_events_url") val receivedEventsUrl: String, //https://api.github.com/users/ReactiveX/received_events
        @SerializedName("type") val type: String, //Organization
        @SerializedName("site_admin") val siteAdmin: Boolean //false
)