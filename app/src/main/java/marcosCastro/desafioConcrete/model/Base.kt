package marcosCastro.desafioConcrete.model

import com.google.gson.annotations.SerializedName

data class Base(
        @SerializedName("label") val label: String, //spring-projects:1.5.x
        @SerializedName("ref") val ref: String, //1.5.x
        @SerializedName("sha") val sha: String, //07c5590813be1834aba9501bb3a236808318fb8a
        @SerializedName("user") val user: Owner,
        @SerializedName("repo") val repo: Repository
)