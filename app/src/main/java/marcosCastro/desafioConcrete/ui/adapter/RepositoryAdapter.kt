package marcosCastro.desafioConcrete.ui.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_repo.*
import marcosCastro.desafioConcrete.R
import marcosCastro.desafioConcrete.model.Repository
import marcosCastro.desafioConcrete.ui.activity.RepositoryActivity
import marcosCastro.desafioConcrete.util.GlideApp
import marcosCastro.desafioConcrete.util.getExtraKeyOwner
import marcosCastro.desafioConcrete.util.getExtraKeyRepoName
import marcosCastro.desafioConcrete.util.openRepositoryActivity
import marcosCastro.desafioConcrete.viewModel.RepositoryViewModel


class RepositoryAdapter(private val list: List<Repository>?, private val repositoryViewModel: RepositoryViewModel?) : RecyclerView.Adapter<RepositoryViewHolder>() {

    override fun getItemCount() = list?.size!!

    override fun onBindViewHolder(holder: RepositoryViewHolder?, position: Int) {
        holder?.bindData(list!![position])
        if (itemCount == position + 2) {
            repositoryViewModel?.getRepository()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoryViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.adapter_repo, parent, false)
        return RepositoryViewHolder(view)
    }

}

class RepositoryViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bindData(repository: Repository?) {
        txtRepoTitle.text = repository?.name
        txtRepoDescription.text = repository?.description
        txtRepoFork.text = repository?.forks.toString()
        txtRepoFav.text = repository?.stargazersCount.toString()
        GlideApp.with(containerView?.context!!).load(repository?.owner?.avatarUrl).placeholder(R.drawable.profile_placeholder).circleCrop().into(imgRepoProfile)

        containerView.setOnClickListener {
            val intent = Intent(this.containerView.context, RepositoryActivity::class.java)
            intent.openRepositoryActivity(repository?.owner?.login, repository?.name, containerView.context)
        }
    }
}