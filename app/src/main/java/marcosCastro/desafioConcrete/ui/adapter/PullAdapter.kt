package marcosCastro.desafioConcrete.ui.adapter

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_pull.*
import marcosCastro.desafioConcrete.R
import marcosCastro.desafioConcrete.model.Pull
import marcosCastro.desafioConcrete.util.GlideApp


class PullAdapter(private val list: List<Pull>?) : RecyclerView.Adapter<PullViewHolder>() {

    override fun getItemCount() = list?.size!!

    override fun onBindViewHolder(holder: PullViewHolder?, position: Int) {
        holder?.bindData(list!![position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PullViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.adapter_pull, parent, false)
        return PullViewHolder(view)
    }

}

class PullViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bindData(pull: Pull?) {
        txtTitle.text = pull?.title
        txtPulllDesc.text = pull?.body
        txtName.text = pull?.user?.login
        GlideApp.with(containerView?.context!!).load(pull?.user?.avatarUrl).placeholder(R.drawable.profile_placeholder).circleCrop().into(imgProfileUser)

        closedView.visibility = if (pull?.isClosed()!!) View.VISIBLE else View.GONE

        containerView.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(pull.htmlUrl))
            containerView.context.startActivity(browserIntent)
        }
    }
}