package marcosCastro.desafioConcrete.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_repository.*
import kotlinx.android.synthetic.main.include_loading.*
import marcosCastro.desafioConcrete.R
import marcosCastro.desafioConcrete.http.ApiResponse
import marcosCastro.desafioConcrete.model.Pull
import marcosCastro.desafioConcrete.ui.adapter.PullAdapter
import marcosCastro.desafioConcrete.util.getExtraKeyOwner
import marcosCastro.desafioConcrete.util.getExtraKeyRepoName
import marcosCastro.desafioConcrete.util.handlerError
import marcosCastro.desafioConcrete.viewModel.PRViewModel

class RepositoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val repoOwner = intent.getStringExtra(getExtraKeyOwner())
        val repoName = intent.getStringExtra(getExtraKeyRepoName())
        val prViewModel = ViewModelProviders.of(this).get(PRViewModel::class.java)

        prViewModel.loadIssues(repoOwner, repoName)

        prViewModel.mApiResponse?.observe(this, Observer {
            if (it?.error == null)
                handlerSuccess(it)
            else
                handlerError()

            viewLoading.visibility = View.GONE
        })
    }

    private fun handlerSuccess(apiResponse: ApiResponse?) {
        var pull = apiResponse?.data!! as List<Pull>
        var opened = 0
        var closed = 0

        for (pull in pull) {

            if (pull.isClosed())
                closed++
            else
                opened++
        }
        txtOpened.text = getString(R.string.txt_repository_opened, opened, closed)
        recPull.layoutManager = LinearLayoutManager(this)
        recPull.adapter = PullAdapter(pull)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        onBackPressed()

        return super.onOptionsItemSelected(item)
    }
}
