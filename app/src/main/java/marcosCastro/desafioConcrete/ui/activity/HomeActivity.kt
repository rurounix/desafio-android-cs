package marcosCastro.desafioConcrete.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.include_loading.*
import marcosCastro.desafioConcrete.R
import marcosCastro.desafioConcrete.model.Repository
import marcosCastro.desafioConcrete.ui.adapter.RepositoryAdapter
import marcosCastro.desafioConcrete.util.handlerError
import marcosCastro.desafioConcrete.viewModel.RepositoryViewModel

class HomeActivity : AppCompatActivity() {
    private lateinit var repositoryViewModel: RepositoryViewModel
    private var adapter: RepositoryAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        repositoryViewModel = ViewModelProviders.of(this).get(RepositoryViewModel::class.java)

        repositoryViewModel?.mApiResponse?.observe(this, Observer {
            if (it?.error == null)
                mountAdapter(it?.data as List<Repository>)
            else
                handlerError()

            viewLoading.visibility = View.GONE
        })
    }

    private fun mountAdapter(response: List<Repository>) {

        if (adapter == null) {
            adapter = RepositoryAdapter(response, repositoryViewModel)
            recRepo.layoutManager = LinearLayoutManager(this)
            recRepo.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }
    }
}
