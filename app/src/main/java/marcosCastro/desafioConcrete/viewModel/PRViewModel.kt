package marcosCastro.desafioConcrete.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import marcosCastro.desafioConcrete.http.ApiResponse
import marcosCastro.desafioConcrete.http.HttpClient

class PRViewModel : ViewModel() {

    var mApiResponse: LiveData<ApiResponse>? = null

    fun loadIssues(user: String, repo: String): LiveData<ApiResponse>? {
        if (mApiResponse == null) {
            mApiResponse = MutableLiveData<ApiResponse>()

            HttpClient.INSTANCE.getJavaPR(user,repo, mApiResponse as MutableLiveData<ApiResponse>)
        }

        return mApiResponse
    }
}

