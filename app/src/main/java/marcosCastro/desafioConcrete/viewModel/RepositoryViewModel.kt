package marcosCastro.desafioConcrete.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import marcosCastro.desafioConcrete.http.ApiResponse
import marcosCastro.desafioConcrete.http.HttpClient
import marcosCastro.desafioConcrete.model.Repository

class RepositoryViewModel : ViewModel() {

    var page = MutableLiveData<Int>()
    var mApiResponse = MutableLiveData<ApiResponse>()
    var values = mutableListOf<Repository>()

    init {
        page.value = 0
        getRepository()
    }

    fun getRepository() {
        page.value = page.value?.plus(1)

        HttpClient.INSTANCE.getRepo(page.value, mApiResponse, values)
    }
}