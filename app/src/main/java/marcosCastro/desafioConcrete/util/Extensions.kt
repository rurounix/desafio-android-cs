package marcosCastro.desafioConcrete.util

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.include_error.*

/**
 * Created by msn-d on 30/01/2018.
 */

fun getExtraKeyOwner() = "EXTRA_KEY.OWNER"

fun getExtraKeyRepoName() = "EXTRA_KEY.REPO_NAME"

fun AppCompatActivity.handlerError(){
    viewError.visibility = View.VISIBLE

}

fun Intent.openRepositoryActivity(login: String?, name: String?, context: Context) {
    putExtra(getExtraKeyOwner(), login)
    putExtra(getExtraKeyRepoName(), name)
    context.startActivity(this)
}