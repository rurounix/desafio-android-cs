package marcosCastro.desafioConcrete.util

/**
 * Created by msn-d on 30/01/2018.
 */
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class AppGlideModule : AppGlideModule()