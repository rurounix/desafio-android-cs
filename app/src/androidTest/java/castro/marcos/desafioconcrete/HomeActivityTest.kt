import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.test.uiautomator.UiDevice
import marcosCastro.desafioConcrete.R
import marcosCastro.desafioConcrete.ui.activity.HomeActivity
import marcosCastro.desafioConcrete.ui.adapter.PullViewHolder
import marcosCastro.desafioConcrete.ui.adapter.RepositoryViewHolder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HomeActivityTest {

    @Rule
    var mActivityTestRule = ActivityTestRule(HomeActivity::class.java)

    @Test
    fun homeActivityTest1() {

        onView(withId(R.id.recRepo)).perform(RecyclerViewActions.actionOnItemAtPosition<RepositoryViewHolder>(0, click()))

        try {
            Thread.sleep(3000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        onView(withId(R.id.recPull))
                .perform(RecyclerViewActions.actionOnItemAtPosition<PullViewHolder>(0, click()))

        try {
            Thread.sleep(3000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        val mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        mDevice.pressBack()

        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        mDevice.pressBack()

        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        mDevice.pressBack()

        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

}